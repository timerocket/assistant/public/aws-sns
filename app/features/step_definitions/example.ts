import { Given, When, Then } from 'cucumber'
import * as expect from 'expect';

Given(/^A dog$/, () => {
  this.dog = { streetSide: 'north'}
});
When(/^It crosses the street$/, () => {
  this.dog.streetSide = 'south'
});
Then(/^It should be on the other side$/, () => {
  expect(this.dog.streetSide).toEqual('south');
});