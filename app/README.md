# AWS SNS

Interface for AWS SNS service

## Setup

### Localise.biz
You need an account with [localize.biz](https://localise.biz) to commit language file updates to this repository

If you haven't done so already add the following to your `~/.bash_profile`

```bash
export TIMEROCKET_ASSISTANT_AWS_SNS_LOCALISE_BIZ_API_KEY=your_api_key_goes_here
```

Reload your bash profile
```bash
source ~/.bash_profile
```

Verify your api key has been set as an environment variable
```bash
env | grep LOCALISE_BIZ_API_KEY
```

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# incremental rebuild (webpack)
$ yarn webpack
$ yarn start:hmr

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Support

No Support

## License

No License
