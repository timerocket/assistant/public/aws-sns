import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { SourceTypeEnum } from './source-type.enum';

@Injectable()
export class SourceTypeEnumPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    const validSourceTypes = Object.values(SourceTypeEnum);
    if (!validSourceTypes.includes(value)) {
      throw new BadRequestException(`Invalid source type: ${value}`);
    }
    return value;
  }
}
