import { SourceTypeEnumPipe } from './source-type-enum.pipe';
import { SourceTypeEnum } from './source-type.enum';

describe('enable-multiple-side-by-side-version', () => {
  it('Should not throw an exception when a valid type is provided', () => {
    const pipe = new SourceTypeEnumPipe();
    const result = pipe.transform(SourceTypeEnum.PINPOINT, null);

    expect(result).toEqual(SourceTypeEnum.PINPOINT);
  });

  it('should throw an exception when an invalid source type provided', () => {
    const pipe = new SourceTypeEnumPipe();

    expect(() => {
      pipe.transform('asdf', null);
    }).toThrow();
  });
});
