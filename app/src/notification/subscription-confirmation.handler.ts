import { NotificationHandlerInterface } from './notification-handler.interface';
import { SnsSubscriptionConfirmationInterface } from './sns-subscription-confirmation.interface';
import { HttpException, HttpStatus } from '@nestjs/common';
import axios from 'axios';
import { Context, SimpleHttpResponse } from '@timerocket/common';
import { i18nData } from '../locale/locales';
import { LocalesEnum } from '../locale/enum';
import { Config } from '../config';

export class SubscriptionConfirmationHandler
  implements NotificationHandlerInterface {
  constructor(
    private readonly notification: SnsSubscriptionConfirmationInterface,
    private readonly config: Config,
  ) {}

  public async handle(context: Context): Promise<any> {
    if (this.config.validateNotificationAuthenticity) {
      await axios.get(this.notification.SubscribeURL).catch((error) => {
        context.logger.log(error);
        return new HttpException(
          'Could not confirm subscription',
          HttpStatus.BAD_REQUEST,
        );
      });
    }
    context.logger.log(`Subscription Confirmed`);

    return new SimpleHttpResponse(
      context,
      HttpStatus.ACCEPTED,
      i18nData,
      LocalesEnum.SUBSCRIPTION_CONFIRMED,
    );
  }
}
