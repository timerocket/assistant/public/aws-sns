import { SubscriptionConfirmationHandler } from './subscription-confirmation.handler';
import { NotificationHandlerInterface } from './notification-handler.interface';
import { NotificationHandler } from './notification.handler';
import { HttpException, HttpStatus, Logger } from '@nestjs/common';
import { Config } from '../config';
import { KafkaService } from '@timerocket/common';
import { MessageTypeEnum } from './message-type.enum';
import { SourceTypeEnum } from '../source/source-type.enum';
import { PhoneNumberChannelEnum } from '@timerocket/data-model';

export class NotificationHandlerFactory {
  constructor(
    private readonly kafkaService: KafkaService,
    private readonly logger: Logger,
    private readonly config: Config,
  ) {}

  public getHandler(
    messageType: MessageTypeEnum,
    sourceType: SourceTypeEnum,
    notification: any,
    phoneNumberChannel?: PhoneNumberChannelEnum,
  ): NotificationHandlerInterface {
    const typeMap = {
      [MessageTypeEnum.SUBSCRIPTION_CONFIRMATION]: SubscriptionConfirmationHandler,
      [MessageTypeEnum.NOTIFICATION]: NotificationHandler,
    };

    if (!typeMap[messageType]) {
      throw new HttpException(
        `Invalid Message Type: ${messageType}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const clazz = typeMap[messageType];

    return new clazz(
      notification,
      this.config,
      this.kafkaService,
      sourceType,
      phoneNumberChannel,
    );
  }
}
