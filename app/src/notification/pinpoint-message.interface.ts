export interface PinpointMessageInterface {
  originationNumber: string;
  destinationNumber: string;
  messageKeyword: string;
  messageBody: string;
  inboundMessageId: string;
  previousPublishedMessageId: string;
}
