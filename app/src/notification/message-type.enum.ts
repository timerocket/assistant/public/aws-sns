export enum MessageTypeEnum {
  SUBSCRIPTION_CONFIRMATION = 'SubscriptionConfirmation',
  NOTIFICATION = 'Notification',
}
