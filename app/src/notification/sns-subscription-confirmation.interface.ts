export interface SnsSubscriptionConfirmationInterface {
  Type: 'SubscriptionConfirmation';
  MessageId: string;
  Token: string;
  TopicArn: string;
  Message: string;
  SubscribeURL;
  Timestamp: string;
  SignatureVersion: string;
  Signature: string;
  SigningCertURL: string;
}
