import { Context } from '@timerocket/common';

export interface NotificationHandlerInterface {
  handle(context: Context): Promise<any>;
}
