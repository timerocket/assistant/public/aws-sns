import {
  MessageInterface,
  MessageMetaInterface,
  MetaTypeEnum,
  PhoneNumberChannelEnum,
  ReplyDataInterface,
  SmsSourceInterface,
} from '@timerocket/data-model';
import { PinpointMessageInterface } from './pinpoint-message.interface';
import { Context, MessageMeta, SmsReply } from '@timerocket/common';

export class PinpointMessage
  implements MessageInterface<ReplyDataInterface<SmsSourceInterface>> {
  data: ReplyDataInterface<SmsSourceInterface>;
  meta: MessageMetaInterface;

  constructor(
    context: Context,
    messageContent: PinpointMessageInterface,
    timestamp: Date,
    type: MetaTypeEnum,
    phoneNumberChannelEnum?: PhoneNumberChannelEnum,
  ) {
    this.meta = new MessageMeta(
      type,
      context.locale,
      context.config,
      messageContent.inboundMessageId,
    );

    // Could be an email reply
    if (messageContent.originationNumber) {
      this.data = new SmsReply(
        messageContent.originationNumber,
        messageContent.destinationNumber,
        phoneNumberChannelEnum,
        messageContent.messageBody,
      );
    }
  }
}
