import { NotificationHandlerInterface } from './notification-handler.interface';
import { SnsNotificationInterface } from './sns-notification.interface';
import { HttpStatus } from '@nestjs/common';
import { PinpointMessage } from './pinpoint-message';
import { MetaTypeEnum, PhoneNumberChannelEnum } from '@timerocket/data-model';
import { Context, KafkaService, SimpleHttpResponse } from '@timerocket/common';
import { i18nData } from '../locale/locales';
import { LocalesEnum } from '../locale/enum';
import { Config } from '../config';
import { SourceTypeEnum } from '../source/source-type.enum';

export class NotificationHandler implements NotificationHandlerInterface {
  constructor(
    private readonly notification: SnsNotificationInterface,
    private readonly config: Config,
    private readonly kafkaService: KafkaService,
    private readonly sourceType: SourceTypeEnum,
    private readonly phoneNumberChannel?: PhoneNumberChannelEnum,
  ) {}

  public async handle(context: Context): Promise<any> {
    const messageContentJSON = this.notification.Message;
    const messageContent = JSON.parse(messageContentJSON);
    const timestamp = new Date(this.notification.Timestamp);
    const type = MetaTypeEnum.REPLY_MESSAGE_SMS;

    let messagePayload;
    if (this.sourceType === SourceTypeEnum.PINPOINT) {
      messagePayload = new PinpointMessage(
        context,
        messageContent,
        timestamp,
        type,
        this.phoneNumberChannel,
      );
    }

    // noinspection ES6MissingAwait
    this.kafkaService.send(messagePayload, context.logger);

    return new SimpleHttpResponse(
      context,
      HttpStatus.ACCEPTED,
      i18nData,
      LocalesEnum.NOTIFICATION_RECEIVED,
    );
  }
}
