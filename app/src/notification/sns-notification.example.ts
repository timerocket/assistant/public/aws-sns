import { MessageTypeEnum } from './message-type.enum';
import { SnsNotificationInterface } from './sns-notification.interface';

export class SnsNotificationExample implements SnsNotificationInterface {
  Type: string = MessageTypeEnum.NOTIFICATION;
  MessageId: string = '70206f35-0915-4855-964f-82429c26cd8e';
  TopicArn: string = 'arn:aws:sns:us-west-2:354762570730:timerocket-entrypoint';
  Message: string =
    '{"originationNumber":"+14072354361","destinationNumber":"+13342491534","messageKeyword":"keyword_354762570730","messageBody":"Sap","inboundMessageId":"d2aaa543-be15-57e8-be51-f2321a6424d6","previousPublishedMessageId":"qnsbv94h5g83eod9gdihfiq43nqps7os2p8ckpo0"}';
  Timestamp: string = '2020-06-27T19:46:57.415Z';
  SignatureVersion: string = '1';
  Signature: string =
    'rVG2+IyDIAAJThDvlTdSZO37XT8Os1clSNtcBW3uyQkkUjetmHQOCfbYqtQASr6wAW6zY4dVoL9dsFs7D/4+xdxBEqs2jkLxTrVUbggZo9qR0Gynl1srzo/jWqE2phYwVdpbPr4LBrEsnJ4Gx16sb6cibakfMyDSrY1zw8SDOO+gvevwibwuZ06vzjuvXm/Pi41fyzUMAdSkk2Pyszkk0z4n5HNUBdHQq71Rk0S7nJdlL7cboIi+fxAzk8GB/votxmUBZbv69AXVr9uCAocU1CGDH3ukWQbRfrYYI4wbWXnfp2jwwczhKcJ6D7InkEcOZWZJhCFIsnIiR97pfjj8/A==';
  SigningCertURL: string =
    'https://sns.us-west-2.amazonaws.com/SimpleNotificationService-a86cb10b4e1f29c941702d737128f7b6.pem';
  UnsubscribeURL: string =
    'https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:354762570730:timerocket-entrypoint:6e66036e-f70c-4414-9102-49cd3b9aa0da';
}
