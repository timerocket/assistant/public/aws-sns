import {
  Controller,
  Logger,
  LoggerService,
  Inject,
  // plagiarize:start:delete
  Body,
  Headers,
  Post,
  Query,
  Req,
  // plagiarize:end:delete
} from '@nestjs/common';
// plagiarize:start:delete
import { AppService } from './app.service';
import * as rawbody from 'raw-body';
import {
  CountryCode,
  LanguageCode,
  Locale,
  MessageContext,
  MessageInterface,
  PhoneNumberChannelEnum,
} from '@timerocket/data-model';
import {
  Context,
  CountryCodePipe,
  LanguageCodePipe,
  NotEmptyPipe,
} from '@timerocket/common';
import {
  ApiAcceptedResponse,
  ApiBody,
  ApiHeader,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
// plagiarize:end:delete
import { Config } from './config';
// plagiarize:start:delete
import { AmazonClient } from './amazon-client';
import { SourceTypeEnumPipe } from './source/source-type-enum.pipe';
import { SourceTypeEnum } from './source/source-type.enum';
import { MessageTypeEnum } from './notification/message-type.enum';
import { SnsNotificationExample } from './notification/sns-notification.example';
import { SnsSubscriptionConfirmationInterface } from './notification/sns-subscription-confirmation.interface';
import { SnsNotificationInterface } from './notification/sns-notification.interface';

// plagiarize:end:delete

@Controller(/* p:s:d */ '/aws/sns' /* p:e:d */)
export class AppController {
  constructor(
    // plagiarize:start:delete
    private readonly appService: AppService,
    // plagiarize:end:delete
    private readonly config: Config,
    @Inject(Logger) private readonly logger: LoggerService,
  ) {}

  // plagiarize:start:delete
  @ApiTags('notification')
  @Post('notification')
  @ApiHeader({
    name: 'x-amz-sns-message-type',
    required: true,
    schema: { example: MessageTypeEnum.NOTIFICATION },
  })
  @ApiHeader({
    name: 'x-amz-sns-message-id',
    required: true,
    schema: { example: '70206f35-0915-4855-964f-82429c26cd8e' },
  })
  @ApiQuery({ name: 'language', required: true, schema: { example: 'eng' } })
  @ApiQuery({ name: 'country', required: true, schema: { example: 'US' } })
  @ApiQuery({
    name: 'sourceType',
    required: true,
    schema: { example: SourceTypeEnum.PINPOINT },
  })
  @ApiQuery({ name: 'contextId', required: false })
  @ApiQuery({ name: 'contextCategory', required: false })
  @ApiQuery({
    name: 'phoneNumberChannel',
    required: false,
    schema: { example: PhoneNumberChannelEnum.ENTRY_POINT },
  })
  @ApiAcceptedResponse({
    description: 'SNS message received',
  })
  @ApiBody({ type: String })
  @ApiBody({
    type: String,
    required: true,
    schema: { example: JSON.stringify(new SnsNotificationExample()) },
  })
  async notification(
    @Headers('x-amz-sns-message-type') messageType: MessageTypeEnum,
    @Headers('x-amz-sns-message-id') correlationId: string,
    @Query('language', NotEmptyPipe, LanguageCodePipe) language: LanguageCode,
    @Query('country', NotEmptyPipe, CountryCodePipe) country: CountryCode,
    @Body()
    data: SnsNotificationInterface | SnsSubscriptionConfirmationInterface,
    @Req() req,
    @Query('sourceType', NotEmptyPipe, SourceTypeEnumPipe)
    sourceType: SourceTypeEnum,
    @Query('phoneNumberChannel')
    phoneNumberChannel?: PhoneNumberChannelEnum | undefined,
    @Query('contextId') contextId?: string | undefined,
    @Query('contextCategory') contextCategory?: string | undefined,
  ): Promise<MessageInterface<string>> {
    const context = new Context(
      correlationId,
      this.logger,
      this.config,
      new AmazonClient(),
      new Locale(language, country),
      new MessageContext(contextCategory, contextId),
    );

    // SNS sends message with header 'Content-Type: text/plain' but this may change in the future and when it does I don't want this to break
    let message: any;
    if (req.readable) {
      const raw = await rawbody(req);
      const text = raw.toString().trim();
      message = JSON.parse(text);
    } else {
      message = data;
    }
    context.logger.log(message);

    try {
      return this.appService.notification(
        context,
        messageType,
        sourceType,
        message,
        phoneNumberChannel,
      );
    } catch (e) {
      context.logger.error(e.message, e.stack);
    }
  }
  // plagiarize:end:delete
}
