import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as packageJSON from '../package.json';
import { Config } from './config';
import { dirname } from 'path';
import { KafkaService } from '@timerocket/common';

async function bootstrap() {
  const config = new Config(dirname(dirname(__dirname)));
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(`${config.appPrefix}/${config.apiVersion}`);

  const docOptions = new DocumentBuilder()
    .setTitle(packageJSON.name)
    .setDescription(packageJSON.description)
    .setVersion(packageJSON.version)
    .build();

  const document = SwaggerModule.createDocument(app, docOptions);
  SwaggerModule.setup(
    `${config.docsPrefix}/${config.apiVersion}`,
    app,
    document,
  );

  const kafkaService = app.get<KafkaService>(KafkaService);
  await kafkaService.startProducer();

  await app.listen(config.httpPort);
}

bootstrap().then();
