export enum LocalesEnum {
  SUBSCRIPTION_CONFIRMED = 'subscription-confirmed',
  NOTIFICATION_RECEIVED = 'notification-received',
}
