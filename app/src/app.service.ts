import { Injectable, Logger } from '@nestjs/common';
import { NotificationHandlerFactory } from './notification/notification-handler.factory';
import { MessageValidator } from 'sns-validator';
import { Context, KafkaService } from '@timerocket/common';
import { SnsSubscriptionConfirmationInterface } from './notification/sns-subscription-confirmation.interface';
import { SnsNotificationInterface } from './notification/sns-notification.interface';
import { Config } from './config';
import { PhoneNumberChannelEnum } from '@timerocket/data-model';
import { SourceTypeEnum } from './source/source-type.enum';
import { MessageTypeEnum } from './notification/message-type.enum';

@Injectable()
export class AppService {
  private readonly logger;
  private readonly factory: NotificationHandlerFactory;
  private readonly validator: MessageValidator;

  constructor(
    private readonly kafkaService: KafkaService,
    private readonly config: Config,
    logger: Logger,
  ) {
    this.validator = new MessageValidator();
    this.logger = logger;
    this.factory = new NotificationHandlerFactory(
      kafkaService,
      this.logger,
      config,
    );
  }

  async notification(
    context: Context,
    messageType: MessageTypeEnum,
    sourceType: SourceTypeEnum,
    notification:
      | SnsSubscriptionConfirmationInterface
      | SnsNotificationInterface,
    phoneNumberChannel?: PhoneNumberChannelEnum,
  ) {
    if (this.config.validateNotificationAuthenticity) {
      await this._authenticateNotification(notification);
    }

    const handler = this.factory.getHandler(
      messageType,
      sourceType,
      notification,
      phoneNumberChannel,
    );

    context.started = new Date(notification.Timestamp);

    return await handler.handle(context);
  }

  private _authenticateNotification(notification: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.validator.validate(notification, (error, message) => {
        if (error) {
          Logger.error(`Authenticity error: ${error}`);
          reject(error);
        } else {
          resolve();
        }
      });
    });
  }
}
