import { Client } from '@timerocket/common';
import { ClientInterface } from '@timerocket/data-model';

export class AmazonClient extends Client implements ClientInterface {
  constructor() {
    super(
      '5c98494f-8d65-4db9-8e35-7d9089688f74',
      '2010-03-31',
      'aws-sns',
      'production',
    );
  }
}
