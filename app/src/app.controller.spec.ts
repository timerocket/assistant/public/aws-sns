import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {
  anObjectLike,
  AppHttpExceptionFilter,
  ConfigBooleanEnum,
  Context,
  CustomLogger,
  KafkaService,
} from '@timerocket/common';
import { APP_FILTER } from '@nestjs/core';
import {
  Locale,
  MessageContext,
  MetaTypeEnum,
  PhoneNumberChannelEnum,
} from '@timerocket/data-model';
import { Logger } from '@nestjs/common';
import { Config } from './config';
import { dirname } from 'path';
import { anything, instance, mock, reset, verify } from 'ts-mockito';
import { SnsNotificationInterface } from './notification/sns-notification.interface';
import { PinpointMessage } from './notification/pinpoint-message';
import { SnsSubscriptionConfirmationInterface } from './notification/sns-subscription-confirmation.interface';
import axios from 'axios';
import { AmazonClient } from './amazon-client';
import { SourceTypeEnum } from './source/source-type.enum';
import MockInstance = jest.MockInstance;

jest.mock('axios');

jest.mock('sns-validator', () => {
  return {
    MessageValidator: jest.fn().mockImplementation(() => {
      return {
        validate: (message, cb) => {
          cb();
        },
      };
    }),
  };
});

describe('AppController', () => {
  let app: TestingModule;
  let KafkaServiceMock: KafkaService;
  let realDateNow;
  let appConfig: Config;

  beforeAll(async () => {
    appConfig = new Config(`${dirname(dirname(__dirname))}`, 'test');

    KafkaServiceMock = mock(KafkaService);
    const kafkaService = instance(KafkaServiceMock);
    const logger = new CustomLogger();

    app = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        AppService,
        {
          provide: APP_FILTER,
          useValue: new AppHttpExceptionFilter(
            new Locale('eng', 'US'),
            logger,
            appConfig,
          ),
        },
        {
          provide: KafkaService,
          useValue: kafkaService,
        },
        {
          provide: Logger,
          useValue: logger,
        },
        {
          provide: Config,
          useValue: appConfig,
        },
      ],
    }).compile();
  });

  beforeEach(() => {
    jest.resetModules();
    reset(KafkaServiceMock);
    realDateNow = Date.now.bind(global.Date);
  });

  afterEach(() => {
    global.Date.now = realDateNow;
    process.env.VALIDATE_NOTIFICATION_AUTHENTICITY = ConfigBooleanEnum.FALSE;
  });

  describe('notification', () => {
    let appController;
    let context;

    beforeAll(() => {
      appController = app.get<AppController>(AppController);

      global.Date.now = jest.fn(() => 1593287218000);

      context = new Context(
        '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
        new CustomLogger(),
        appConfig,
        new AmazonClient(),
        new Locale('eng', 'US'),
        new MessageContext(),
      );
    });

    describe('type: Notification', () => {
      let pinpointMessage;
      let notificationBody: SnsNotificationInterface;

      beforeAll(() => {
        notificationBody = {
          Type: 'Notification',
          MessageId: '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          TopicArn: 'arn:aws:sns:us-west-2:123456789012:MyTopic',
          Subject: 'My First Message',
          Message:
            '{"originationNumber":"+14072354361","destinationNumber":"+13342491534","messageKeyword":"keyword_354762570730","messageBody":"Sap","inboundMessageId":"d2aaa543-be15-57e8-be51-f2321a6424d6","previousPublishedMessageId":"qnsbv94h5g83eod9gdihfiq43nqps7os2p8ckpo0"}',
          Timestamp: '2020-06-27T19:46:57.415Z',
          SignatureVersion: '1',
          Signature:
            'rVG2+IyDIAAJThDvlTdSZO37XT8Os1clSNtcBW3uyQkkUjetmHQOCfbYqtQASr6wAW6zY4dVoL9dsFs7D/4+xdxBEqs2jkLxTrVUbggZo9qR0Gynl1srzo/jWqE2phYwVdpbPr4LBrEsnJ4Gx16sb6cibakfMyDSrY1zw8SDOO+gvevwibwuZ06vzjuvXm/Pi41fyzUMAdSkk2Pyszkk0z4n5HNUBdHQq71Rk0S7nJdlL7cboIi+fxAzk8GB/votxmUBZbv69AXVr9uCAocU1CGDH3ukWQbRfrYYI4wbWXnfp2jwwczhKcJ6D7InkEcOZWZJhCFIsnIiR97pfjj8/A==',
          SigningCertURL:
            'https://sns.us-west-2.amazonaws.com/SimpleNotificationService-a86cb10b4e1f29c941702d737128f7b6.pem',
          UnsubscribeURL:
            'https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:354762570730:timerocket-entrypoint:6e66036e-f70c-4414-9102-49cd3b9aa0da"}',
        } as SnsNotificationInterface;

        const pinpointMessageBody = {
          originationNumber: '+14072354361',
          destinationNumber: '+13342491534',
          messageKeyword: 'keyword_354762570730',
          messageBody: 'Sap',
          inboundMessageId: 'd2aaa543-be15-57e8-be51-f2321a6424d6',
          previousPublishedMessageId:
            'qnsbv94h5g83eod9gdihfiq43nqps7os2p8ckpo0',
        };

        pinpointMessage = new PinpointMessage(
          context,
          pinpointMessageBody,
          new Date(notificationBody.Timestamp),
          MetaTypeEnum.REPLY_MESSAGE_SMS,
          PhoneNumberChannelEnum.ENTRY_POINT,
        );
      });

      it('should send message to kafka service', async () => {
        await appController.notification(
          'Notification',
          '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          'eng',
          'US',
          notificationBody,
          { readable: false },
          SourceTypeEnum.PINPOINT,
          PhoneNumberChannelEnum.ENTRY_POINT,
        );

        verify(
          KafkaServiceMock.send(anObjectLike(pinpointMessage), anything()),
        ).times(1);
      });

      it('should return an ok response', async () => {
        const result = await appController.notification(
          'Notification',
          '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          'eng',
          'US',
          notificationBody,
          { readable: false },
          SourceTypeEnum.PINPOINT,
          PhoneNumberChannelEnum.ENTRY_POINT,
        );

        const expectedResponseBody = {
          data: 'Notification received',
          meta: {
            client: {
              id: appConfig.clientId,
              name: 'aws-sns',
              variant: appConfig.environmentName,
              version: '0.3.2',
            },
            context: {
              category: 'default',
              id: 'none',
            },
            correlationId: '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
            interactionId: null,
            locale: {
              country: 'US',
              language: 'eng',
            },
            schemaVersion: '0.1.0',
            time: {
              created: '2020-06-27T19:46:58.000Z',
              started: '2020-06-27T19:46:57.415Z',
            },
            type: 'na.http.simple',
          },
        };

        expect(JSON.parse(JSON.stringify(result))).toStrictEqual(
          expectedResponseBody,
        );
      });
    });

    describe('type: SubscriptionConfirmation', () => {
      let notificationBody: SnsSubscriptionConfirmationInterface;

      beforeAll(() => {
        notificationBody = {
          Type: 'SubscriptionConfirmation',
          MessageId: '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          Token: '2336412f37f...',
          TopicArn: 'arn:aws:sns:us-west-2:123456789012:MyTopic',
          Message:
            'You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\\nTo confirm the subscription, visit the SubscribeURL included in this message.',
          SubscribeURL:
            'https://sns.us-west-2.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-west-2:123456789012:MyTopic&Token=2336412f37...',
          Timestamp: '2020-06-27T19:46:57.415Z',
          SignatureVersion: '1',
          Signature:
            'rVG2+IyDIAAJThDvlTdSZO37XT8Os1clSNtcBW3uyQkkUjetmHQOCfbYqtQASr6wAW6zY4dVoL9dsFs7D/4+xdxBEqs2jkLxTrVUbggZo9qR0Gynl1srzo/jWqE2phYwVdpbPr4LBrEsnJ4Gx16sb6cibakfMyDSrY1zw8SDOO+gvevwibwuZ06vzjuvXm/Pi41fyzUMAdSkk2Pyszkk0z4n5HNUBdHQq71Rk0S7nJdlL7cboIi+fxAzk8GB/votxmUBZbv69AXVr9uCAocU1CGDH3ukWQbRfrYYI4wbWXnfp2jwwczhKcJ6D7InkEcOZWZJhCFIsnIiR97pfjj8/A==',
          SigningCertURL:
            'https://sns.us-west-2.amazonaws.com/SimpleNotificationService-a86cb10b4e1f29c941702d737128f7b6.pem',
        } as SnsSubscriptionConfirmationInterface;
      });

      it('should confirm the subscription', async () => {
        process.env.VALIDATE_NOTIFICATION_AUTHENTICITY = ConfigBooleanEnum.TRUE;

        // @ts-ignore
        const get = axios.get as MockInstance<any, any[]>;
        get.mockResolvedValue({});

        await appController.notification(
          'SubscriptionConfirmation',
          '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          'eng',
          'US',
          notificationBody,
          { readable: false },
          SourceTypeEnum.PINPOINT,
          PhoneNumberChannelEnum.ENTRY_POINT,
        );

        expect(axios.get).toBeCalledTimes(1);
      });

      it('should return an ok response', async () => {
        const result = await appController.notification(
          'SubscriptionConfirmation',
          '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
          'eng',
          'US',
          notificationBody,
          { readable: false },
          SourceTypeEnum.PINPOINT,
          PhoneNumberChannelEnum.ENTRY_POINT,
        );

        const expectedResponseBody = {
          data: 'Subscription confirmed',
          meta: {
            client: {
              id: appConfig.clientId,
              name: 'aws-sns',
              variant: appConfig.environmentName,
              version: '0.3.2',
            },
            context: {
              category: 'default',
              id: 'none',
            },
            correlationId: '92edbdce-cd4d-4730-9466-59eecc1a3bc0',
            interactionId: null,
            locale: {
              country: 'US',
              language: 'eng',
            },
            schemaVersion: '0.1.0',
            time: {
              created: '2020-06-27T19:46:58.000Z',
              started: '2020-06-27T19:46:57.415Z',
            },
            type: 'na.http.simple',
          },
        };

        expect(JSON.parse(JSON.stringify(result))).toStrictEqual(
          expectedResponseBody,
        );
      });
    });
  });
});
