import { ConfigBooleanEnum, DefaultConfig } from '@timerocket/common';
// aws-sns
export class Config extends DefaultConfig {
  // region plagiarize:start:delete
  public get validateNotificationAuthenticity(): boolean {
    return (
      process.env.VALIDATE_NOTIFICATION_AUTHENTICITY === ConfigBooleanEnum.TRUE
    );
  }
  //endregion plagiarize:end:delete
}
