import { Logger, Module } from '@nestjs/common';
import { AppController } from './app.controller';
// plagiarize:start:delete
import { AppService } from './app.service';
// plagiarize:end:delete
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import {
  AppHttpExceptionFilter,
  CustomLogger,
  HttpStatusInterceptor,
  KafkaService,
  KafkaStubService,
} from '@timerocket/common';
import { Config } from './config';
import { Locale } from '@timerocket/data-model';
import { HealthzController } from './healthz.controller';
import { dirname } from 'path';

const appConfig = new Config(`${dirname(dirname(__dirname))}`, 'localhost');
const baseLogger = new CustomLogger(
  'info',
  appConfig.logLevels,
  false,
  new Logger(),
);
const defaultLocale = new Locale(
  appConfig.fallbackLanguage,
  appConfig.fallbackCountry,
);
const kafkaService = appConfig.kafka.enabled
  ? new KafkaService(appConfig)
  : new KafkaStubService();

const appFilter = new AppHttpExceptionFilter(
  defaultLocale,
  baseLogger,
  appConfig,
);

@Module({
  imports: [],
  controllers: [AppController, HealthzController],
  providers: [
    // plagiarize:start:delete
    AppService,
    // plagiarize:end:delete
    {
      provide: APP_FILTER,
      useValue: appFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useValue: new HttpStatusInterceptor(),
    },
    {
      provide: KafkaService,
      useValue: kafkaService,
    },
    {
      provide: Logger,
      useValue: baseLogger,
    },
    {
      provide: Locale,
      useValue: defaultLocale,
    },
    {
      provide: Config,
      useValue: appConfig,
    },
  ],
})
export class AppModule {}
