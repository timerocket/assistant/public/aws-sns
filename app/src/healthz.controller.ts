import { Controller, Get, HttpStatus } from '@nestjs/common';
import { HealthzResponse } from '@timerocket/common';
import { Locale } from '@timerocket/data-model';
import { Config } from './config';

@Controller('/healthz')
export class HealthzController {
  constructor(
    private readonly fallbackLocale: Locale,
    private readonly config: Config,
  ) {}

  @Get()
  public root() {
    return new HealthzResponse(HttpStatus.OK, this.fallbackLocale, this.config);
  }
}
