#!/bin/bash -e
curl \
--header "x-amz-sns-message-type: Notification" \
--header "x-amz-sns-message-id: 22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324" \
--header "x-amz-sns-topic-arn: arn:aws:sns:us-west-2:123456789012:MyTopic" \
--header "Content-Type: text/plain; charset=UTF-8" \
--header "User-Agent: Amazon Simple Notification Service Agent" \
--request POST \
'http://localhost:3000/api/v1/aws/sns/notification?country=US&language=eng' \
--data '{"Type":"Notification","MessageId":"22b80b92-fdea-4c2c-8f9d-bdfb0c7bf324","TopicArn":"arn:aws:sns:us-west-2:354762570730:timerocket-entrypoint","Message":"{\"originationNumber\":\"+14072354361\",\"destinationNumber\":\"+13342491534\",\"messageKeyword\":\"keyword_354762570730\",\"messageBody\":\"Sap\",\"inboundMessageId\":\"d2aaa543-be15-57e8-be51-f2321a6424d6\",\"previousPublishedMessageId\":\"qnsbv94h5g83eod9gdihfiq43nqps7os2p8ckpo0\"}","Timestamp":"2020-06-27T19:46:57.415Z","SignatureVersion":"1","Signature":"rVG2+IyDIAAJThDvlTdSZO37XT8Os1clSNtcBW3uyQkkUjetmHQOCfbYqtQASr6wAW6zY4dVoL9dsFs7D/4+xdxBEqs2jkLxTrVUbggZo9qR0Gynl1srzo/jWqE2phYwVdpbPr4LBrEsnJ4Gx16sb6cibakfMyDSrY1zw8SDOO+gvevwibwuZ06vzjuvXm/Pi41fyzUMAdSkk2Pyszkk0z4n5HNUBdHQq71Rk0S7nJdlL7cboIi+fxAzk8GB/votxmUBZbv69AXVr9uCAocU1CGDH3ukWQbRfrYYI4wbWXnfp2jwwczhKcJ6D7InkEcOZWZJhCFIsnIiR97pfjj8/A==","SigningCertURL":"https://sns.us-west-2.amazonaws.com/SimpleNotificationService-a86cb10b4e1f29c941702d737128f7b6.pem","UnsubscribeURL":"https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:354762570730:timerocket-entrypoint:6e66036e-f70c-4414-9102-49cd3b9aa0da"}'