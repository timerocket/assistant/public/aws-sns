# Environment files

This directory is should be symlinked from `../k8s/aws-sns/env`

## Create a symlink in k8s helm chart
To create a symlink

```bash
cd ../k8s/aws-sns/
ln -s ../../env env
```