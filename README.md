# TimeRocket AWS SNS Service

Takes messages from AWS SNS service webhooks and publishes them to a kafka topic

## Running

### Docker

```bash
cp docker-compose.override.example.yml docker-compose.override.yml
docker-compose up -d zookeeper kafka
docker-compose build
docker-compose up aws-sns
```

### Localhost

```bash
cp docker-compose.override.localhost.yml docker-compose.override.yml
cd app
yarn start:dev
```

### Minikube

```bash
cd k8s
skaffold dev
```

Note* You might get an error along the lines of this `"There is no leader for this topic-partition as we are in the 
middle of a leadership election"` the very first time you send a message. That is ok. 

## Testing

### End to end testing

#### Send a test message
```bash
./bin/test-sns-message.sh
```

#### Send a test subscription confirmation
```bash
./bin/test-sns-subscription-confirmation.sh
```

### Unit testing
```bash
cd app
yarn test
```

## Contributors
- Josh Woodcock

## LICENSE

MIT