{{- $nodeMongoFullName := include "nodejs.fullname" . -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "nodejs.fullname" . }}{{ if .Values.service.canary }}-canary-{{ .Values.service.canary }}{{end}}{{ if .Values.namespace }}
  namespace: {{ .Values.namespace }}{{ end }}
  labels:
{{ include "nodejs.labels" . | indent 4 }}
{{- with .Values.annotations }}
  annotations: {{- toYaml . | nindent 4 }}
{{- end }}
spec:
  selector:
    matchLabels: {{ include "nodejs.labels" . | nindent 6 }}{{ if .Values.service.canary }}
      app.cryptexlabs.com/canary:{{ .Values.service.canary }}{{ end }}
  replicas: {{ if .Values.service.canary}}{{.Values.service.canaryReplicas}}{{else}}{{ .Values.autoscaling.replicas.min }}{{end}}
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:{{ include "nodejs.labels" . | nindent 8 }}{{ if .Values.service.canary }}
        app.cryptexlabs.com/canary:{{ .Values.service.canary }}{{ end }}{{- with .Values.annotations }}
      annotations: {{- toYaml . | nindent 8 }}
      {{- end }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets: {{- toYaml . | nindent 8 }}
      {{- end }}{{ if .Values.rbac.enabled }}
      serviceAccountName: {{ include "nodejs.fullname" . }}{{ end }}
      containers:
        - name: {{ .Chart.Name }}
          {{ if .Values.image.image }}
          image: "{{ .Values.image.image }}"{{else}}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"{{ .Values.image.image }}{{ end }}{{ if .Values.container }}{{ if .Values.container.command }}
          command:{{- range .Values.container.command }}
            - {{ . }}{{ end }}{{ end }}{{ end }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          resources:
            requests:
              cpu: {{ .Values.autoscaling.resources.requests.cpu }}
              memory: {{ .Values.autoscaling.resources.requests.memory }}
            limits:
              cpu: {{ .Values.autoscaling.resources.limits.cpu }}
              memory: {{ .Values.autoscaling.resources.limits.memory }}
          ports:
            - name: "http"
              containerPort: 80

          {{ if .Values.probes.enabled }}
          startupProbe:{{- toYaml .Values.probes.startup | nindent 12 }}
          readinessProbe:{{- toYaml .Values.probes.readiness | nindent 12 }}
          livenessProbe:{{- toYaml .Values.probes.liveness | nindent 12 }}
          {{ end }}

          env:
            - name: APP_VERSION
              value: "{{ .Values.image.tag }}"
            {{- range .Values.env.valueFrom }}
            - name: {{ .var }}
              valueFrom:
                configMapKeyRef:
                  name: {{ $nodeMongoFullName }}-{{ .name }}
                  key: {{ .key }}
            {{- end }}
            {{- range .Values.env.secretFrom }}
            - name: {{ .var }}
              valueFrom:
                secretKeyRef:
                  name: {{ $nodeMongoFullName }}-{{ .name }}
                  key: {{ .key }}
            {{- end }}
            {{- range .Values.env.value }}
            - name: {{ .name }}
              value: "{{ .value }}"
            {{- end }}{{ if .Values.env.from }}
          envFrom:{{ if or .Values.env.from }}{{- toYaml .Values.env.from | nindent 12 }}{{ end }}{{ end }}{{ if or .Values.fileMounts .Values.secretFileMounts }}
          volumeMounts:{{ end }}
          {{- range .Values.fileMounts }}
          {{- range .mounts }}
            - name: {{ .name }}
              mountPath: {{ .fileDirectory }}
              subPath: {{ .fileName }}
          {{- end }}
          {{- end }}
          {{- range .Values.secretFileMounts }}
          {{- $secretName := .name -}}
          {{- range .mounts }}
            - name: {{ $secretName }}
              mountPath: {{ .fileDirectory }}
          {{- end }}
          {{- end }}{{ if or .Values.fileMounts .Values.secretFileMounts }}
      volumes: {{ end }}
      {{- range .Values.fileMounts }}
        - name: {{ .volumeName }}
          configMap:
            name: {{ $nodeMongoFullName }}-{{ .configMap }}
      {{- end }}

      {{- range .Values.secretFileMounts }}
        - name: {{ .name }}
          secret:
            secretName: {{ $nodeMongoFullName }}-{{ .name }}
            defaultMode: 256
            items:
            {{- range .mounts }}
              - key: {{ .fileName }}
                path: {{ .fileName }}
            {{- end }}
      {{- end }}